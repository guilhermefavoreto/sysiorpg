﻿
using System.Collections.ObjectModel;
using Jarloo.CardStock.Models;
using System.Xml.Linq;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            // var saldo = new Historico();
            var operacao = new Conta();
            var minhaLista = new List<Historico>();
            var meuHistorico = new Historico();
            //var conversor = new Conversor();

            
            operacao.operaEntrada("Arma", "Espada", meuHistorico, minhaLista);
            operacao.operaEntrada("Arma", "Maça", meuHistorico, minhaLista);
            operacao.operaEntrada("Arma", "Lança", meuHistorico, minhaLista);
            operacao.operaEntrada("Armadura", "Lança", meuHistorico, minhaLista);
            operacao.operaEntrada("Armadura", "Lança", meuHistorico, minhaLista);
            //conversor.operaConversao("$", "R$", 1M, meuHistorico, minhaLista);
            //conversor.operaConversao("R$", "$", 1M, meuHistorico, minhaLista);
            //operacao.operaSaida("R$", 1M, meuHistorico, minhaLista);
            meuHistorico.geraHistorico(minhaLista);
        }
    }
}
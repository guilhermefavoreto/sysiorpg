﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Historico
    {
        public DateTime Data { get; set; }
        public decimal Cotacao { get; set; }
        public String Valor { get; set; }
        public String TipoOperacao { get; set; }
        public String Simbolo { get; set; }
        public String Tipo { get; set; }
        public int operacaoID { get; set; }

        public void geraHistorico(List<Historico> lista)
        {
            var operacoesEntrada = lista.Where(c => c.TipoOperacao == "Entrada");
            var operacoesSaida = lista.Where(c => c.TipoOperacao == "Saída");
            var operacoesConversaoRealDollar = lista.Where(c => c.operacaoID == 2);
            var operacoesConversaoDollarReal = lista.Where(c => c.operacaoID == 1);
            Random random = new Random();
            int arma = 0;
            int armadura = 0;
            
            foreach (Historico deposito in operacoesEntrada.Where(c => c.Tipo == "Arma"))
            {
                arma++;
                if(arma == 1)
                    Console.Write("Este é seu inventário de Armas : \n");
                Console.WriteLine(deposito.Valor + ", Valor da Arma: " + random.Next(1, 100));                
            }
        
            Console.WriteLine();
            
            foreach (Historico deposito in operacoesEntrada.Where(c => c.Tipo == "Armadura"))
            {
                armadura++;
                if(armadura == 1)
                    Console.Write("Este é seu inventário de Armaduras : \n");
                Console.WriteLine(deposito.Valor + ", Valor da Armadura: " + random.Next(1, 100));
            }

            foreach (Historico saque in operacoesSaida.Where(c => c.Tipo == "Arma"))
            {
                arma++;
                if(arma == 1)
                    Console.Write("Estes são seus saques: ");
                Console.WriteLine(saque.Valor +  ", Valor da Arma" + saque.Data);
            }

            foreach (Historico saque in operacoesSaida.Where(c => c.Tipo == "Armadura"))
            {
                arma++;
                if (arma == 1)
                    Console.Write("Estes são seus saques: ");
                Console.WriteLine(saque.Valor + ", Valor da Armadura" + saque.Data);
            }

            foreach (Historico conversao in operacoesConversaoRealDollar)
            {
                //Console.Write("Estas são suas conversões de Real para Dóllar: ");
                Console.WriteLine(conversao.TipoOperacao + ", " + conversao.Simbolo + conversao.Valor + ", " + conversao.Data + ", " + conversao.Cotacao);
            }

            foreach (Historico conversao in operacoesConversaoDollarReal)
            {
                //Console.Write("Estas são suas conversões de Dóllar para Real: ");
                Console.WriteLine(conversao.TipoOperacao + ", " + conversao.Simbolo + conversao.Valor + ", " + conversao.Data + ", " + conversao.Cotacao);
            }
        }   
    }
}
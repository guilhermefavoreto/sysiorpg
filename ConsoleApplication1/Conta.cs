﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Conta
    {
        public void operaSaida(string tipo, String valorSaida, Historico saldo, List<Historico> lista)
        {
            if (tipo == "Arma")
            {
                //saldo.Valor = saldo.Valor - valorSaida;
                var operacao = new Historico()
                {
                    Valor = valorSaida,
                    Data = DateTime.Now,
                    Tipo = tipo,
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }
            if (tipo == "Armadura")
            {
                //saldo.Valor = saldo.Valor - valorSaida;
                var operacao = new Historico()
                {
                    Valor = valorSaida,
                    Data = DateTime.Now,
                    Tipo = tipo,
                    TipoOperacao = "Saída"
                };
                lista.Add(operacao);
            }
        }

        public void operaEntrada(string tipo, string valorEntrada, Historico saldo, List<Historico> lista)
        {           
            if (tipo == "Arma")
            {
                //saldo.Valor = valorEntrada + saldo.Valor;
                var operacao = new Historico()
                {
                    Valor = valorEntrada,
                    Data = DateTime.Now,
                    Tipo = tipo,
                    TipoOperacao = "Entrada"
                };
                lista.Add(operacao);
            }

            if (tipo == "Armadura")
            {
                //saldo.Valor = valorEntrada + saldo.Valor;
                var operacao = new Historico()
                {
                    Valor = valorEntrada,
                    Data = DateTime.Now,
                    Tipo = tipo,
                    TipoOperacao = "Entrada",
                    //Cotacao = saldo.Cotacao
                };
                lista.Add(operacao);
            }          
        }

        //public decimal checaSaldo(string tipo, Historico meuSaldoReal, Historico meuSaldoDollar)
        //{
        //    if (tipo == "$")
        //        return meuSaldoDollar.Valor;
        //    else
        //        return meuSaldoReal.Valor;
        //}
    }
}
